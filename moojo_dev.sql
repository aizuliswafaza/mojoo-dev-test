-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table moojo_dev.mst_user
CREATE TABLE IF NOT EXISTS `mst_user` (
  `id` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table moojo_dev.mst_user: ~7 rows (approximately)
/*!40000 ALTER TABLE `mst_user` DISABLE KEYS */;
INSERT INTO `mst_user` (`id`, `username`, `password`, `fullname`, `picture`, `created_at`, `updated_at`) VALUES
	('05c9f4b0-26fd-4982-b234-90e9340e9304', 'ultramen', '$2a$10$D/iKa6uSWCtZeV4clpt3kuglVwmxeqaisechZICxeFIFHr0gWuJ9W', 'ULTRAMEN GAIA', 'ini picture.jpg', '2021-01-27 05:39:19', '2021-01-27 05:39:19'),
	('186bc1cd-8b6c-448b-a31a-a867aa11fa76', 'ultramen', '$2a$10$Vdn4Ue1ieBWs5apZWdloL.CIhfk3WnDg9NjpR9H3jTV3bk41RHVHm', 'ULTRAMEN GAIA', 'ini picture.jpg', '2021-01-27 05:39:04', '2021-01-27 05:39:04'),
	('271cbe01-5826-4e71-ae8e-d68e57295bc9', 'boneka', '123452', 'boneka beruang', 'ini picture.jpg', '2021-01-27 05:18:30', '2021-01-27 05:18:30'),
	('9bb09c0f-48c1-42ed-bf93-45a50a222445', 'aizul25', '123457', 'aizul iswafaza', 'ini picture.jpg', '2021-01-27 04:35:17', '2021-01-27 04:35:53'),
	('b5d655a0-acb4-4178-a4f9-2517b2fdba8e', 'boneka', '123452', 'boneka beruang', 'ini picture.jpg', '2021-01-27 04:42:24', '2021-01-27 04:42:24'),
	('ba210230-fa06-4df8-9630-4bdf6fae094c', 'halu', 'halu123', 'Aku Halu Sekali', '1f82b2e8918f4a29816cbeecec517396.jpg', '2021-01-27 05:06:51', '2021-01-27 05:06:51'),
	('c0872e1d-5afb-41a1-aa87-654d01c6b3b6', 'ultramen', '$2a$10$vmI556D1LnYUm53qSGfnkOyF1DoB6/twTI0hbknJkdKuCl7XdjgQ.', 'ULTRAMEN GAIA', 'ini picture.jpg', '2021-01-27 05:34:01', '2021-01-27 05:34:01');
/*!40000 ALTER TABLE `mst_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
