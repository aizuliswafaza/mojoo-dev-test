package com.moojo.devtest.config;

import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"${server.error.path:${error.path:/error}}"})
public class BasicErrorControllerOverride extends AbstractErrorController {
    public BasicErrorControllerOverride(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @RequestMapping
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        HttpStatus status = this.getStatus(request);
        Map<String, Object> errorCustomAttribute = new HashMap<>();
        errorCustomAttribute.put("message", status.name());
        return new ResponseEntity(errorCustomAttribute, status);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
