package com.moojo.devtest.config;

import com.moojo.devtest.exceptions.FileStorageException;
import com.moojo.devtest.exceptions.MyFileNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class StorageConfig {

    private static final Logger LOG = LogManager.getLogger(StorageConfig.class);

    @Autowired
    StorageConfigProperties storageConfigProperties;

    public String storeFile(String folderName, MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        String[] splitFilename = filename.split("\\.");
        try {
            if(filename.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + filename);
            }

            if(splitFilename.length < 2){
                throw new FileStorageException("Sorry! We Cannot Find File extension on " + filename);
            }

            Path fileStorageLocation = Paths.get((storageConfigProperties.getPath() + folderName))
                    .toAbsolutePath().normalize();

            try {
                LOG.info("Start findOrCreate dir");
                Files.createDirectories(fileStorageLocation);
                LOG.info("finish findOrCreate dir");
            } catch (Exception ex) {
                LOG.info("error findOrCreate dir");
                throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
            }

            String ext = splitFilename[splitFilename.length - 1];
            String randomFilename = UUID.randomUUID().toString().replace("-", "");
            filename = randomFilename + "." + ext;

            Path targetLocation = fileStorageLocation.resolve(filename);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return filename;
        }catch (Exception ex){
            throw new FileStorageException("Could not store file " + filename + ". Please try again!", ex);
        }
    }

    public Resource loadFile(String fileName) {
        try {
            Path fileStorageLocation = Paths.get((storageConfigProperties.getPath()))
                    .toAbsolutePath().normalize();
            Path filePath = fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            LOG.info("" + filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
//                throw new MyFileNotFoundException("File is not found " + fileName);
                return null;
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File is not found " + fileName, ex);
        }
    }
}