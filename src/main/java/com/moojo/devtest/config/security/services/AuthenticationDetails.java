package com.moojo.devtest.config.security.services;

import com.moojo.devtest.models.MasterUser;
import com.moojo.devtest.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class AuthenticationDetails {
    @Autowired
    HttpServletRequest request;

    @Autowired
    UserService userService;

    private MasterUser masterUser;

    private String id;

    private void instance(){
        String id = request.getUserPrincipal().getName();
        this.masterUser = userService.findById(id);
        this.id = id;
    }

    public String getUserId(){
        return id;
    }

    public MasterUser getUser(){
        this.instance();
        return masterUser;
    }
}
