package com.moojo.devtest.config.security.services;

import com.moojo.devtest.models.MasterUser;
import com.moojo.devtest.models.QMasterUser;
import com.moojo.devtest.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private final QMasterUser qData = QMasterUser.masterUser;

	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		MasterUser user = userRepository.findOne(qData.username.eq(username))
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

		return UserDetailsImpl.build(user);
	}

	@Transactional
	public UserDetailsImpl loadUserById(String id) throws UsernameNotFoundException {
		MasterUser user = userRepository.findOne(qData.id.eq(id))
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with id: " + id));

		return UserDetailsImpl.build(user);
	}

	public String getTokenData(String token){
		MasterUser user = userRepository.findOne(qData.id.eq(token))
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with this token: " + token));
		return null;
	}
}
