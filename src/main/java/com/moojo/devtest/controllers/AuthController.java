package com.moojo.devtest.controllers;

import com.moojo.devtest.exceptions.BadRequestErrorException;
import com.moojo.devtest.exceptions.InternalServerErrorException;
import com.moojo.devtest.forms.AuthForm;
import com.moojo.devtest.forms.UserAddForm;
import com.moojo.devtest.repositories.UserRepository;
import com.moojo.devtest.responses.BaseResponse;
import com.moojo.devtest.services.AuthService;
import com.moojo.devtest.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private static final Logger LOG = LogManager.getLogger(AuthController.class);

    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody @Valid AuthForm authForm, BindingResult br){
        if(br.hasErrors()){
            String field = br.getFieldError().getField();
            String msg = br.getFieldErrors().get(0).getDefaultMessage();
            LOG.error(field + ": " + msg);
            return BadRequestErrorException.build("Parameter " + field + " harus diisi");
        }

        try {
            BaseResponse baseResponse = authService.login(authForm);
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }

    @PostMapping("/sign-up")
    public ResponseEntity create(@RequestBody @Valid UserAddForm userAddForm, BindingResult br){
        if(br.hasErrors()){
            String field = br.getFieldError().getField();
            String msg = br.getFieldErrors().get(0).getDefaultMessage();
            LOG.error(field + ": " + msg);
            return BadRequestErrorException.build("Parameter " + field + " harus diisi");
        }

        try {
            BaseResponse baseResponse = userService.create(userAddForm);
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }

    @PostMapping("/me")
    @PreAuthorize("hasRole('ALL')")
    public ResponseEntity login(){
        try {
            BaseResponse baseResponse = authService.me();
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }

    @PostMapping("/mobile/logout")
    @PreAuthorize("hasRole('MOBILE')")
    public ResponseEntity logout(){
        try {
            BaseResponse baseResponse = authService.logout();
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }
}