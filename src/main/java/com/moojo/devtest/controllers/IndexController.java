package com.moojo.devtest.controllers;

import com.moojo.devtest.responses.BaseResponse;
import com.moojo.devtest.utils.DateUtil;
import com.moojo.devtest.utils.FormatDateUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/")
public class IndexController {

    @GetMapping
    public ResponseEntity datatable(){

        System.out.println(ServletUriComponentsBuilder.fromCurrentContextPath().toUriString());
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setMessage("Welcome To Moojo DEV API 1.0");
        baseResponse.setData(FormatDateUtil.formatted(DateUtil.now_with_zone(), "dd-MMMM-yyyy HH:mm"));
        return baseResponse.build();
    }
}
