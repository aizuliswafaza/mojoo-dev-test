package com.moojo.devtest.controllers;

import com.moojo.devtest.exceptions.BadRequestErrorException;
import com.moojo.devtest.exceptions.InternalServerErrorException;
import com.moojo.devtest.forms.FileUploadForm;
import com.moojo.devtest.responses.BaseFileResponse;
import com.moojo.devtest.responses.BaseResponse;
import com.moojo.devtest.services.StorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/storage")
public class StorageController {

    private static final Logger LOG = LogManager.getLogger(StorageController.class);

    @Autowired
    StorageService storageService;

    @PostMapping("/upload")
    @PreAuthorize("hasRole('ALL')")
    public ResponseEntity upload(@RequestBody @ModelAttribute @Valid FileUploadForm fileUploadForm, BindingResult br){
        if(br.hasErrors()){
            String field = br.getFieldError().getField();
            String msg = br.getFieldErrors().get(0).getDefaultMessage();
            LOG.error(field + ": " + msg);
            return BadRequestErrorException.build("Parameter " + field + " harus diisi");
        }

        try {
            BaseResponse baseResponse = storageService.upload(fileUploadForm);
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }

    @GetMapping("/{folderName}/{fileName:.+}")
    public ResponseEntity<Resource> get(@PathVariable String folderName, @PathVariable String fileName, HttpServletRequest request) {
        try {
            String uri = folderName + "/" + fileName;
            BaseFileResponse baseFileResponse = storageService.get(uri, request);

            return baseFileResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }
}