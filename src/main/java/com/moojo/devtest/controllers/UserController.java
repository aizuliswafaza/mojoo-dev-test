package com.moojo.devtest.controllers;

import com.moojo.devtest.exceptions.BadRequestErrorException;
import com.moojo.devtest.exceptions.InternalServerErrorException;
import com.moojo.devtest.forms.UserAddForm;
import com.moojo.devtest.forms.UserAddWithPictureForm;
import com.moojo.devtest.forms.UserEditForm;
import com.moojo.devtest.forms.UserEditWithPictureForm;
import com.moojo.devtest.models.MasterUser;
import com.moojo.devtest.queryFilters.UserQueryFilter;
import com.moojo.devtest.responses.BaseResponse;
import com.moojo.devtest.responses.JQueryDataTableResponse;
import com.moojo.devtest.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/master-user")
public class UserController {
    private static final Logger LOG = LogManager.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @GetMapping("/read")
    @PreAuthorize("hasRole('ALL')")
    public ResponseEntity datatable(@Valid UserQueryFilter qf){
        Page<MasterUser> page = userService.read(qf);

        JQueryDataTableResponse<MasterUser> jQueryDataTableResponse = new JQueryDataTableResponse<>();
        jQueryDataTableResponse.setDraw(qf.getDraw());
        jQueryDataTableResponse.setRecordsTotal(page.getTotalElements());
        jQueryDataTableResponse.setRecordsFiltered(page.getTotalElements());
        jQueryDataTableResponse.setData(page.getContent());

        return jQueryDataTableResponse.build();
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ALL')")
    public ResponseEntity create(@RequestBody @Valid UserAddForm userAddForm, BindingResult br){
        if(br.hasErrors()){
            String field = br.getFieldError().getField();
            String msg = br.getFieldErrors().get(0).getDefaultMessage();
            LOG.error(field + ": " + msg);
            return BadRequestErrorException.build("Parameter " + field + " harus diisi");
        }

        try {
            BaseResponse baseResponse = userService.create(userAddForm);
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }

    @PostMapping("/create-with-picture")
    @PreAuthorize("hasRole('ALL')")
    public ResponseEntity createWithPicture(@RequestBody @ModelAttribute @Valid UserAddWithPictureForm userAddWithPictureForm, BindingResult br){
        if(br.hasErrors()){
            String field = br.getFieldError().getField();
            String msg = br.getFieldErrors().get(0).getDefaultMessage();
            LOG.error(field + ": " + msg);
            return BadRequestErrorException.build("Parameter " + field + " harus diisi");
        }

        try {
            BaseResponse baseResponse = userService.createWithPicture(userAddWithPictureForm);
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }

    @PutMapping("/update")
    @PreAuthorize("hasRole('ALL')")
    public ResponseEntity update(@RequestBody @Valid UserEditForm userEditForm, BindingResult br){
        if(br.hasErrors()){
            String field = br.getFieldError().getField();
            String msg = br.getFieldErrors().get(0).getDefaultMessage();
            LOG.error(field + ": " + msg);
            return BadRequestErrorException.build("Parameter " + field + " harus diisi");
        }

        try {
            BaseResponse baseResponse = userService.update(userEditForm);
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }

    @PostMapping("/update-with-picture")
    @PreAuthorize("hasRole('ALL')")
    public ResponseEntity updateWithPicture(@RequestBody @ModelAttribute @Valid UserEditWithPictureForm UserEditWithPictureForm, BindingResult br){
        if(br.hasErrors()){
            String field = br.getFieldError().getField();
            String msg = br.getFieldErrors().get(0).getDefaultMessage();
            LOG.error(field + ": " + msg);
            return BadRequestErrorException.build("Parameter " + field + " harus diisi");
        }

        try {
            BaseResponse baseResponse = userService.updateWithPicture(UserEditWithPictureForm);
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ALL')")
    public ResponseEntity update(@PathVariable String id){
        if(id.isEmpty() || id.isBlank()){
            LOG.error("id: is empty");
            return BadRequestErrorException.build("Parameter id harus diisi");
        }

        try {
            BaseResponse baseResponse = userService.delete(id);
            return baseResponse.build();
        }catch (Exception ex){
            ex.printStackTrace();
            return InternalServerErrorException.build();
        }
    }
}
