package com.moojo.devtest.exceptions;

import com.moojo.devtest.responses.ReturnMessageResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BadRequestErrorException {
    public static ResponseEntity build(String message){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(new ReturnMessageResponse(message));
    }
}
