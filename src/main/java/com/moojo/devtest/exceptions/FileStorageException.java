package com.moojo.devtest.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileStorageException extends RuntimeException {
    private static final Logger LOG = LogManager.getLogger(FileStorageException.class);

    public FileStorageException(String message) {
        super(message);
        LOG.info(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
