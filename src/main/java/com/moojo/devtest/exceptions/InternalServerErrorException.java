package com.moojo.devtest.exceptions;

import com.moojo.devtest.responses.ReturnMessageResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class InternalServerErrorException {
    public static ResponseEntity build(){
        String message = "Terjadi kesalahan pada sistem kami, coba beberapa saat lagi.";
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(new ReturnMessageResponse(message));
    }
}
