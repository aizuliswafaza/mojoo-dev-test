package com.moojo.devtest.forms;

import javax.validation.constraints.NotNull;

public class BaseEditForm extends BaseForm {

    @NotNull
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
