package com.moojo.devtest.forms;

import com.moojo.devtest.utils.Constant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FileForm extends BaseForm{
    private String filename;
    private String pathname;
    private String url;
    private String filetype;
    private Long size;

    public FileForm(MultipartFile file){
        this.filetype = file.getContentType();
        this.size = file.getSize();
    }

    public void setUrl(String foldername){
        this.url = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(Constant.SERVER_BOX_PATH + foldername + "/")
                .path(filename)
                .toUriString()
                .replace( Constant.APP_CODE +"/","");
        this.pathname = foldername + "/" + filename;
    }
}
