package com.moojo.devtest.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FileUploadForm extends BaseForm{

    @NotNull
    private MultipartFile file;

    @NotNull
    private String foldername;
}
