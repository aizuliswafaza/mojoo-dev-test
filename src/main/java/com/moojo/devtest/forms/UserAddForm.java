package com.moojo.devtest.forms;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class UserAddForm extends BaseForm {

    @NotNull
    private String username;
    @NotNull
    private String password;
    private String fullname;
    private String picture;
}
