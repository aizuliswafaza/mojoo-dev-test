package com.moojo.devtest.forms;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class UserAddWithPictureForm extends BaseForm {
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private MultipartFile file;

    private String fullname;
}
