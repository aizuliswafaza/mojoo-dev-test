package com.moojo.devtest.forms;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class UserEditWithPictureForm extends BaseEditForm {
    @NotNull
    private String username;
    @NotNull
    private String password;
    private String fullname;

    @NotNull
    private MultipartFile file;
}
