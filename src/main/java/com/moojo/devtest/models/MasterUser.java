package com.moojo.devtest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.moojo.devtest.models.hooks.BaseHook;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "mst_user")
@EntityListeners(BaseHook.class)
public class MasterUser extends BaseModel{

    private String username;

    @JsonIgnore
    private String password;

    private String fullname;

    private String picture;
}
