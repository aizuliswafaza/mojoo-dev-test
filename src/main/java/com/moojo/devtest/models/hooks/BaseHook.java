package com.moojo.devtest.models.hooks;

import com.moojo.devtest.models.BaseModel;
import com.moojo.devtest.utils.DateUtil;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class BaseHook {

    @PrePersist
    public void methodExecuteBeforeSave(final BaseModel model){
        model.setCreated_at(DateUtil.now_with_zone());
        model.setUpdated_at(DateUtil.now_with_zone());
    }

    @PreUpdate
    public void methodExecuteBeforeUpdate(final BaseModel model){
        model.setUpdated_at(DateUtil.now_with_zone());
    }

}
