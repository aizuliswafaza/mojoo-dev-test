package com.moojo.devtest.queryFilters;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class BaseQueryFilter {

    private String id = null;
    private Integer draw = 1;
    private Integer page = 0;
    private Integer length = 10;
    private String search_text = null;
    private List<String> sort = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getSearch_text() {
        return search_text;
    }

    public void setSearch_text(String search_text) {
        this.search_text = search_text;
    }

    public List<String> getSort() {
        return sort;
    }

    public void setSort(List<String> sort) {
        this.sort = sort;
    }

    public Pageable pageable() {
        return PageRequest.of(getPage(), getLength());
    }
    
    public int getStart() {

        if (getPage() > 0 && getLength() > 0) {
            return (getPage() - 1) * getLength();
        }

        return 0;
    }
}
