package com.moojo.devtest.repositories;

import com.moojo.devtest.models.MasterUser;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<MasterUser, String>, QuerydslPredicateExecutor<MasterUser> {

}
