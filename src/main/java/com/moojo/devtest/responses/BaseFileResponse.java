package com.moojo.devtest.responses;

import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Getter
@Setter
public class BaseFileResponse extends BaseResponse{

    private static final Logger LOG = LogManager.getLogger(BaseFileResponse.class);

    private String content_type = "application/octet-stream";

    private Resource resource;

    public ResponseEntity build(){
        if(this.getStatus() == HttpStatus.OK.value()){
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(this.content_type))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + this.resource.getFilename() + "\"")
                    .body(this.resource);
        }else{
            return super.build();
        }
    }

    public void setContent_type(HttpServletRequest request){
        try {
            String temp_content_type = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            this.content_type = temp_content_type;
        } catch (IOException ex) {
            // Fallback to the default content type if type could not be determined
            LOG.info("Could not determine file type.");
        }
    }
}
