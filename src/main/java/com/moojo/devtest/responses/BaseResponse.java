package com.moojo.devtest.responses;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Getter
@Setter
public class BaseResponse {

    private Integer status = HttpStatus.OK.value();

    private String message;

    private String messageDev;

    private Object data;

    public ResponseEntity build(){
        return ResponseEntity.status(this.status).body(this);
    }
}
