package com.moojo.devtest.responses;

import org.springframework.http.ResponseEntity;

import java.util.Collection;
import java.util.Collections;

public class JQueryDataTableResponse<T> {

    private int status = 200;
    private int draw = 1;
    private long recordsTotal = 0;
    private long recordsFiltered = 0;
    private Collection<T> data = Collections.emptyList();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public Collection<T> getData() {
        return data;
    }

    public void setData(Collection<T> data) {
        this.data = data;
    }

    public ResponseEntity build(){
        return ResponseEntity.status(this.status).body(this);
    }
}
