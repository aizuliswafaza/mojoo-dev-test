package com.moojo.devtest.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.moojo.devtest.models.MasterUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

@Setter
@Getter
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private MasterUser masterUser;

	@Value("${telemed.app.jwtExpirationMs}")
	private int exp;

	@JsonIgnore
	private String id;

	@JsonIgnore
	private String username;

	@JsonIgnore
	private String email;

	@JsonIgnore
	private List<String> roles;

	public JwtResponse(String accessToken, MasterUser masterUser){
		this.token = accessToken;
		this.masterUser = masterUser;
	}

	public JwtResponse(String accessToken, String id, String username, String email, List<String> roles) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
	}
}
