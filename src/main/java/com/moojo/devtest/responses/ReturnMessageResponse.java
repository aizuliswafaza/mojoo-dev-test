package com.moojo.devtest.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ReturnMessageResponse {
    private String message;
}
