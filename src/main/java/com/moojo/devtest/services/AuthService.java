package com.moojo.devtest.services;

import com.moojo.devtest.forms.AuthForm;
import com.moojo.devtest.responses.BaseResponse;

public interface AuthService {

    BaseResponse login(AuthForm authForm);

    BaseResponse me();

    BaseResponse logout();
}
