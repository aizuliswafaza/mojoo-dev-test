package com.moojo.devtest.services;

import com.moojo.devtest.forms.FileUploadForm;
import com.moojo.devtest.responses.BaseFileResponse;
import com.moojo.devtest.responses.BaseResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface StorageService {

    BaseResponse upload(FileUploadForm fileUploadForm);

    BaseFileResponse get(String filename, HttpServletRequest request);
}
