package com.moojo.devtest.services;

import com.moojo.devtest.forms.UserAddForm;
import com.moojo.devtest.forms.UserAddWithPictureForm;
import com.moojo.devtest.forms.UserEditForm;
import com.moojo.devtest.forms.UserEditWithPictureForm;
import com.moojo.devtest.models.MasterUser;
import com.moojo.devtest.queryFilters.UserQueryFilter;
import com.moojo.devtest.responses.BaseResponse;
import org.springframework.data.domain.Page;

public interface UserService {
    Page<MasterUser> read (UserQueryFilter qf);
    BaseResponse create (UserAddForm userAddForm);
    BaseResponse createWithPicture (UserAddWithPictureForm userAddWithPictureForm);
    BaseResponse update (UserEditForm userEditForm);
    BaseResponse updateWithPicture (UserEditWithPictureForm userEditWithPictureForm);
    BaseResponse delete (String id);
    MasterUser findById(String id);
}
