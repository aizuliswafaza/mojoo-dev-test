package com.moojo.devtest.services.impl;

import com.moojo.devtest.config.security.jwt.JwtUtils;
import com.moojo.devtest.config.security.services.AuthenticationDetails;
import com.moojo.devtest.config.security.services.UserDetailsImpl;
import com.moojo.devtest.forms.AuthForm;
import com.moojo.devtest.models.MasterUser;
import com.moojo.devtest.repositories.UserRepository;
import com.moojo.devtest.responses.BaseResponse;
import com.moojo.devtest.responses.JwtResponse;
import com.moojo.devtest.services.AuthService;
import com.moojo.devtest.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class AuthServiceImpl implements AuthService {

    private static final Logger LOG = LogManager.getLogger(AuthServiceImpl.class);

    @Autowired
    AuthenticationDetails authenticateDetails;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Override
    public BaseResponse login(AuthForm authForm){
        LOG.info("start login user mobile");
        BaseResponse baseResponse = new BaseResponse();
        JwtResponse jwtResponse = this.login(authForm.getUsername(), authForm.getPassword());

        if(jwtResponse == null){
            baseResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            baseResponse.setMessage("hmmm diriku tidak tau kenapa ini null, tapi coba lagi aja ya~");

            return baseResponse;
        }

        LOG.info("end login user mobile");
        baseResponse.setData(jwtResponse);
        return baseResponse;
    }

    private JwtResponse login(String username, String password){
        return this.login(username, password, null);
    }

    private JwtResponse login(String username, String password, MasterUser masterUser){
        LOG.info("try login user");
        MasterUser temp = masterUser;
        try{
            UsernamePasswordAuthenticationToken temp_token = new UsernamePasswordAuthenticationToken(username, password);
            Authentication authentication = authenticationManager.authenticate(temp_token);
            LOG.info("success auth function");

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);
            
            LOG.info("success create token");

            if(temp == null){
                UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
                temp = userService.findById(userDetails.getId());
            }

            JwtResponse jwtResponse = new JwtResponse(jwt, temp);

            LOG.info("success login user");
            return jwtResponse;
        }catch(Exception ex){
            LOG.info(ex.getMessage());
        }

        LOG.info("failed login user");
        return null;
    }

    @Override
    public BaseResponse me(){
        BaseResponse baseResponse = new BaseResponse();
        MasterUser masterUser = authenticateDetails.getUser();
        baseResponse.setData(masterUser);
        return baseResponse;
    }

    @Override
    public BaseResponse logout(){
        BaseResponse baseResponse = new BaseResponse();
        MasterUser masterUser = authenticateDetails.getUser();
        userRepository.save(masterUser);

        baseResponse.setMessage("Berhasil logout :D");
        return baseResponse;
    }
}
