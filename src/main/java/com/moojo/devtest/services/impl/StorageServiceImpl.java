package com.moojo.devtest.services.impl;

import com.moojo.devtest.config.StorageConfig;
import com.moojo.devtest.forms.FileForm;
import com.moojo.devtest.forms.FileUploadForm;
import com.moojo.devtest.responses.BaseFileResponse;
import com.moojo.devtest.responses.BaseResponse;
import com.moojo.devtest.services.StorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StorageServiceImpl implements StorageService {

    private static final Logger LOG = LogManager.getLogger(StorageServiceImpl.class);

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    StorageConfig storageConfig;

    @Override
    public BaseResponse upload(FileUploadForm fileUploadForm) {
        LOG.info("start upload file");
        BaseResponse baseResponse = new BaseResponse();

        String filename = storageConfig.storeFile(fileUploadForm.getFoldername(), fileUploadForm.getFile());

        FileForm form = new FileForm(fileUploadForm.getFile());
        form.setFilename(filename);
        form.setUrl(fileUploadForm.getFoldername());

        baseResponse.setData(form);
        LOG.info("end upload file");
        return baseResponse;
    }

    @Override
    public BaseFileResponse get(String filename, HttpServletRequest request) {
        LOG.info("start get file");
        BaseFileResponse baseFileResponse = new BaseFileResponse();

        Resource resource = storageConfig.loadFile(filename);

        if(resource == null){
            baseFileResponse.setStatus(HttpStatus.NOT_FOUND.value());
            baseFileResponse.setMessage("File is not found " + filename);
            return baseFileResponse;
        }

        baseFileResponse.setResource(resource);
        baseFileResponse.setContent_type(request);

        LOG.info("end get file");
        return baseFileResponse;
    }
}
