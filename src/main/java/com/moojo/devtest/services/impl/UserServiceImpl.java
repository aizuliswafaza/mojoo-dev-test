package com.moojo.devtest.services.impl;

import com.moojo.devtest.config.StorageConfig;
import com.moojo.devtest.forms.UserAddForm;
import com.moojo.devtest.forms.UserAddWithPictureForm;
import com.moojo.devtest.forms.UserEditForm;
import com.moojo.devtest.forms.UserEditWithPictureForm;
import com.moojo.devtest.models.MasterUser;
import com.moojo.devtest.models.QMasterUser;
import com.moojo.devtest.queryFilters.UserQueryFilter;
import com.moojo.devtest.repositories.UserRepository;
import com.moojo.devtest.responses.BaseResponse;
import com.moojo.devtest.services.UserService;
import com.moojo.devtest.utils.CustomPageImpl;
import com.querydsl.core.QueryResults;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private QMasterUser qData = QMasterUser.masterUser;

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    StorageConfig storageConfig;

    @Autowired
    UserRepository userRepository;

    @Override
    public Page<MasterUser> read(UserQueryFilter qf) {
        JPAQuery<MasterUser> query = new JPAQuery<MasterUser>(entityManager).from(qData)
                .limit(qf.getLength())
                .offset(qf.getStart());

        if(StringUtils.hasLength(qf.getSearch_text())){
            query.where(qData.username.containsIgnoreCase(qf.getSearch_text())
                    .or(qData.fullname.containsIgnoreCase(qf.getSearch_text())));
        }

        if(StringUtils.hasLength(qf.getId())){
            query.where(qData.id.eq(qf.getId()));
        }

        query.orderBy(qData.updated_at.desc());

        QueryResults<MasterUser> results = query.fetchResults();
        return new CustomPageImpl<>(results.getResults(), qf.pageable(), results.getTotal());
    }

    @Override
    public BaseResponse create(UserAddForm userAddForm) {
        BaseResponse baseResponse = new BaseResponse();
        MasterUser masterUser = new MasterUser();

        masterUser.setUsername(userAddForm.getUsername());
        masterUser.setFullname(userAddForm.getFullname());
        String password_encoded = encoder.encode(userAddForm.getPassword());
        masterUser.setPassword(password_encoded);
        masterUser.setPicture(userAddForm.getPicture());

        userRepository.save(masterUser);

        baseResponse.setData(masterUser);
        return baseResponse;
    }

    @Override
    public BaseResponse createWithPicture(UserAddWithPictureForm userAddWithPictureForm) {
        BaseResponse baseResponse = new BaseResponse();
        MasterUser masterUser = new MasterUser();

        masterUser.setUsername(userAddWithPictureForm.getUsername());
        masterUser.setFullname(userAddWithPictureForm.getFullname());
        String password_encoded = encoder.encode(userAddWithPictureForm.getPassword());
        masterUser.setPassword(password_encoded);

        String filename = storageConfig.storeFile("user-profile", userAddWithPictureForm.getFile());
        masterUser.setPicture(filename);

        userRepository.save(masterUser);

        baseResponse.setData(masterUser);
        return baseResponse;
    }

    @Override
    public BaseResponse update(UserEditForm userEditForm) {
        BaseResponse baseResponse = new BaseResponse();

        MasterUser masterUser = this.findById(userEditForm.getId());

        if(masterUser == null){
            baseResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            baseResponse.setMessage("Data tidak ditemukan");

            return baseResponse;
        }

        masterUser.setUsername(userEditForm.getUsername());
        masterUser.setFullname(userEditForm.getFullname());
        String password_encoded = encoder.encode(userEditForm.getPassword());
        masterUser.setPassword(password_encoded);
        masterUser.setPicture(userEditForm.getPicture());

        userRepository.save(masterUser);

        baseResponse.setData(masterUser);
        return baseResponse;
    }

    @Override
    public BaseResponse updateWithPicture(UserEditWithPictureForm userEditWithPictureForm) {
        BaseResponse baseResponse = new BaseResponse();
        MasterUser masterUser = this.findById(userEditWithPictureForm.getId());

        if(masterUser == null){
            baseResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            baseResponse.setMessage("Data tidak ditemukan");

            return baseResponse;
        }

        masterUser.setUsername(userEditWithPictureForm.getUsername());
        masterUser.setFullname(userEditWithPictureForm.getFullname());
        String password_encoded = encoder.encode(userEditWithPictureForm.getPassword());
        masterUser.setPassword(password_encoded);

        String filename = storageConfig.storeFile("user-profile", userEditWithPictureForm.getFile());
        masterUser.setPicture(filename);

        userRepository.save(masterUser);

        baseResponse.setData(masterUser);
        return baseResponse;
    }

    @Override
    public BaseResponse delete(String id) {
        BaseResponse baseResponse = new BaseResponse();

        MasterUser masterUser = this.findById(id);

        if(masterUser == null){
            baseResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            baseResponse.setMessage("Data tidak ditemukan");

            return baseResponse;
        }

        userRepository.delete(masterUser);

        baseResponse.setMessage("Data " + masterUser.getFullname() + ", berhasil dihapus!");
        return baseResponse;
    }

    @Override
    public MasterUser findById(String id) {
        Optional<MasterUser> userOptional = userRepository.findOne(qData.id.eq(id));
        if(userOptional.isPresent()){
            return userOptional.get();
        }

        return null;
    }
}
