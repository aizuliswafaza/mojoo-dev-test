package com.moojo.devtest.utils;

import java.util.Arrays;
import java.util.List;

public class Constant {

    public static final String APP_NAME = "Telemedicine Service";
    public static final String APP_CODE = "telemedicine";
    public static final String APP_VERSION = "1.0.0";

    public static final String SERVER_BOX_PATH = "/api/storage/";
}
