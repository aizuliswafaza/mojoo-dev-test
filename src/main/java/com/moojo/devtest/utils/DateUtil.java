package com.moojo.devtest.utils;

import java.sql.Time;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.*;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;
import static java.time.temporal.TemporalAdjusters.nextOrSame;
import static java.time.temporal.TemporalAdjusters.previousOrSame;

public class DateUtil {

    public static ZonedDateTime now_with_zone(){
        LocalDateTime ldt = LocalDateTime.now();

        return ldt.atZone(ZoneId.of("Asia/Jakarta"));
    }
}
