package com.moojo.devtest.utils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class FormatDateUtil {

    public static String formatted(ZonedDateTime zonedDateTime, String format){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return zonedDateTime.format(formatter);
    }
}
